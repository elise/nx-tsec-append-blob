# NX TSEC Append Blob

A tool to append blobs to a TSEC FW and edit the size of the boot blob.

## Installation

### Prerequesites

* [Rust](https://rustup.rs)

```sh
cargo install --git https://gitlab.com/elise/nx-tsec-append-blob.git
```

## Usage

```sh
nx-tsec-append-blob <original_firmware> <new_blob> <out>
```

## Credits

* Contributors to the Switchbrew Wiki
