use std::path::PathBuf;

use clap::Parser;

#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error("File not found: {0}")]
    FileNotFound(PathBuf),
    #[error("IO Error: {0}")]
    Io(#[from] std::io::Error),
}

#[derive(Debug, clap::Parser)]
pub struct Args {
    pub original_firmware: PathBuf,
    pub new_blob: PathBuf,
    pub out: PathBuf,
    #[clap(short, long)]
    pub replace_boot: bool,
    #[clap(short, long)]
    pub boot_size: Option<u32>,
}

fn align_up(x: usize, y: usize) -> usize {
    (x + y - 1) & !(y - 1)
}

fn actual_main() -> Result<(), Error> {
    let args = Args::parse();

    if !args.original_firmware.is_file() {
        return Err(Error::FileNotFound(args.original_firmware));
    }

    if !args.new_blob.is_file() {
        return Err(Error::FileNotFound(args.new_blob));
    }

    let original_fw = std::fs::read(args.original_firmware.as_path())?;
    let mut new_blob = std::fs::read(args.new_blob.as_path())?;

    let out_fw = if !args.replace_boot {
        let new_len = align_up(new_blob.len(), 0x100);
        new_blob.resize(new_len, 0);

        let mut out_fw = original_fw;
        let new_len = align_up(out_fw.len(), 0x100);
        out_fw.resize(new_len, 0);
        out_fw.append(&mut new_blob);

        if let Some(x) = args.boot_size {
            let split = x.to_le_bytes();
            out_fw[0x300 + 0x70] = split[0];
            out_fw[0x300 + 0x71] = split[1];
            out_fw[0x300 + 0x72] = split[2];
            out_fw[0x300 + 0x73] = split[3];
        }

        println!("Entry point is {:#X}", new_len);
        out_fw
    } else {
        let new_len = new_blob.len();
        if new_len > 0x300 {
            panic!("New blob too long");
        }

        let mut out_fw = original_fw;
        for x in 0..0x300 {
            out_fw[x] = if x < new_len { new_blob[x] } else { 0 };
        }

        let split = align_up(new_len, 0x100).to_le_bytes();
        out_fw[0x300 + 0x70] = split[0];
        out_fw[0x300 + 0x71] = split[1];
        out_fw[0x300 + 0x72] = split[2];
        out_fw[0x300 + 0x73] = split[3];

        println!("Entry point is 0x0");

        out_fw
    };

    std::fs::write(args.out.as_path(), out_fw)?;

    Ok(())
}

fn main() {
    if let Err(why) = actual_main() {
        eprintln!("{}", why);
    }
}

#[cfg(test)]
mod tests {
    use super::align_up;

    #[test]
    fn align() {
        assert_eq!(align_up(0x3000, 0x100), 0x3000);
        assert_eq!(align_up(0x3001, 0x100), 0x3100);
        assert_eq!(align_up(0x3999, 0x100), 0x3A00);
        assert_eq!(align_up(0, 0x100), 0);
    }
}
